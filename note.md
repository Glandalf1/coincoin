# note à propos de Git

## Créer un dépôt local Git depuis VS code

> git init

- ouvrir vs code dans un dossier donné
- cliquez sur la petite icône VCS et cliquer sur "Initialize Repository"

## Je modifie un fichier que je veux commit

> Je fais un "git add"

- créer/modifier notre fichier, puis l'enregistrer
- Il apparait alors dans la liste des fichiers modifiés de l'onglet VCS (qui nous affiche une notif indiquant le nombre de fichiers modifiés)
- je peux cliquer un par un sur chaque fichier à ajouter OU tout ajouter d'un coup en cliquant sur le bouton `+` à côté de l'option `Changes`

## Commit, kesseussé ?

> git commit -m "coucou les lapinous"

Cela permet de sauvegarder sur MON ordi les modifications que j'ai "add" comme étant une "nouvelle version" au sens git du terme.

- se placer dans l'onglet VCS si on n'y est pas déjà
- vérifier que l'on a tous les fichiers souhaités dans "Staged changes"
- renseigne le message de commit dans la petite fenêtre dédiée, au dessus de la liste des fichiers "staged"
- puis on clique sur `ctrl` + `entrée`
